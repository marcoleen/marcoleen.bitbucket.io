var annotated_dup =
[
    [ "closedloop", null, [
      [ "ClosedLoop", "classclosedloop_1_1ClosedLoop.html", "classclosedloop_1_1ClosedLoop" ]
    ] ],
    [ "driver", null, [
      [ "DRV8847", "classdriver_1_1DRV8847.html", "classdriver_1_1DRV8847" ],
      [ "Motor", "classdriver_1_1Motor.html", "classdriver_1_1Motor" ]
    ] ],
    [ "encoder", null, [
      [ "Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "imu", null, [
      [ "BNO055", "classimu_1_1BNO055.html", "classimu_1_1BNO055" ]
    ] ],
    [ "shares", null, [
      [ "Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ],
      [ "Share", "classshares_1_1Share.html", "classshares_1_1Share" ]
    ] ],
    [ "task_calibration", null, [
      [ "Task_Calibration", "classtask__calibration_1_1Task__Calibration.html", "classtask__calibration_1_1Task__Calibration" ]
    ] ],
    [ "task_controller", null, [
      [ "Task_Controller", "classtask__controller_1_1Task__Controller.html", "classtask__controller_1_1Task__Controller" ]
    ] ],
    [ "task_driver", null, [
      [ "Task_Driver", "classtask__driver_1_1Task__Driver.html", "classtask__driver_1_1Task__Driver" ]
    ] ],
    [ "task_encoder", null, [
      [ "Task_Encoder", "classtask__encoder_1_1Task__Encoder.html", "classtask__encoder_1_1Task__Encoder" ]
    ] ],
    [ "task_imu", null, [
      [ "Task_IMU", "classtask__imu_1_1Task__IMU.html", "classtask__imu_1_1Task__IMU" ]
    ] ],
    [ "task_panel", null, [
      [ "Task_Panel", "classtask__panel_1_1Task__Panel.html", "classtask__panel_1_1Task__Panel" ]
    ] ],
    [ "task_user", null, [
      [ "Task_User", "classtask__user_1_1Task__User.html", "classtask__user_1_1Task__User" ]
    ] ],
    [ "task_user_lab0x04", null, [
      [ "Task_User", "classtask__user__lab0x04_1_1Task__User.html", "classtask__user__lab0x04_1_1Task__User" ]
    ] ],
    [ "touchpanel", null, [
      [ "Panel", "classtouchpanel_1_1Panel.html", "classtouchpanel_1_1Panel" ]
    ] ]
];