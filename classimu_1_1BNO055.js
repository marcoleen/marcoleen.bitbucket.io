var classimu_1_1BNO055 =
[
    [ "__init__", "classimu_1_1BNO055.html#a0a645d173422a380d9f69b18c4f2515b", null ],
    [ "change_mode", "classimu_1_1BNO055.html#a27fd41e82b3f6e9aa25a9a26274681f6", null ],
    [ "read_angles", "classimu_1_1BNO055.html#a55e9c9c8b67dc471c581fd8cb57db76e", null ],
    [ "read_velocity", "classimu_1_1BNO055.html#a4be42dbb56e6270dbff125306d33edc0", null ],
    [ "retrieve_calibration", "classimu_1_1BNO055.html#ac344fbbbdee3742644d7ee03814bb05c", null ],
    [ "retrieve_coefficients", "classimu_1_1BNO055.html#af6904c3cdfae185b4d9487b4b5052ed6", null ],
    [ "write_coefficients", "classimu_1_1BNO055.html#a1711a3abba8d1f33f1fd3ed1b2339595", null ]
];