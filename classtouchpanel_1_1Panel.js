var classtouchpanel_1_1Panel =
[
    [ "__init__", "classtouchpanel_1_1Panel.html#a46c981ab6be0870f2c7869e0f790911d", null ],
    [ "calibrate", "classtouchpanel_1_1Panel.html#a5a969551a3ec308aa00f0217a0f7de70", null ],
    [ "get_values", "classtouchpanel_1_1Panel.html#a8c74731ee93e5dc1af371a1fac9c1949", null ],
    [ "read_x", "classtouchpanel_1_1Panel.html#ae943374fd29a38ca49c6dc4463e47131", null ],
    [ "read_y", "classtouchpanel_1_1Panel.html#a9c346bf7eae1dd82ceacb9bc20844253", null ],
    [ "read_z", "classtouchpanel_1_1Panel.html#ae57572dc71c5f3add55d9c2db53f715c", null ],
    [ "set_calibration", "classtouchpanel_1_1Panel.html#a7e425929c191f9a293ad88bc819a75d5", null ]
];